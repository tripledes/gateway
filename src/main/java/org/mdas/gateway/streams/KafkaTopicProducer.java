package org.mdas.gateway.streams;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.jboss.logging.Logger;
import org.mdas.gateway.model.Measure;
import org.mdas.gateway.model.Sensor;

import io.smallrye.reactive.messaging.kafka.KafkaRecord;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.bind.JsonbBuilder;

/**
 * Producer implementation using Kafka
 */
@ApplicationScoped
public class KafkaTopicProducer implements Producer {
    private static final Logger LOG = Logger.getLogger(KafkaTopicProducer.class);

    // Emit measures to measure topic
    @Inject
    @Channel("measure-topic")
    Emitter<String> measures;

    // Emit sensors to sensor topic
    @Inject
    @Channel("sensor-topic")
    Emitter<String> sensors;

    /**
     * produceMeasure generates KafkaRecords out of measures
     * 
     * @param measure A Measure object serialized to JSON.
     */
    @Override
    public void produceMeasure(Measure measure) {
        if (measure != null) {
            LOG.infov("temperature: {0}, takenAt: {1} by sensor: {2}", measure.getTemperature(), measure.getTakenAt(),
                    measure.getMetadata().getId());

            this.measures.send(KafkaRecord.of(measure.getMetadata().getId(), JsonbBuilder.create().toJson(measure)));
            return;
        }
        LOG.infov("Absent measure");
    }

    /**
     * produceSensor generates KafkaRecords out of sensor objects
     * 
     * @param sensor A Sensor object serialized to JSON.
     */
    @Override
    public void produceSensor(Sensor sensor) {
        if (sensor != null) {
            LOG.infov("area: {0}, id: {1}", sensor.getArea(), sensor.getId());

            this.sensors.send(KafkaRecord.of(sensor.getId(), JsonbBuilder.create().toJson(sensor)));
            return;
        }
        LOG.infov("Absent sensor data");
    }
}
