package org.mdas.gateway.streams;

import org.mdas.gateway.model.Measure;
import org.mdas.gateway.model.Sensor;

/**
 * Producer interface
 */
public interface Producer {

    void produceMeasure(Measure measure);
    void produceSensor(Sensor metadata);

}
