package org.mdas.gateway.controllers;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.metrics.MetricUnits;
import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.metrics.annotation.Timed;
import org.mdas.gateway.model.Sensor;
import org.mdas.gateway.streams.Producer;

/**
 * SensorResource registers sensors
 */
@Path("/register")
@Consumes(MediaType.APPLICATION_JSON)
public class SensorResource {
    @Inject
    Producer producer;

    /**
     * registerSensor gets a Sensor by POST and produces a message with it.
     * 
     * @param sensor A Sensor object, serialized to JSON and stored it on a registry
     *               like topic.
     * @return An HTTP Response object
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Counted(name = "performedChecks", description = "How many sensors has been created.")
    @Timed(name = "checksTimer", description = "A measure of how long it takes to perform the "
            + "operation.", unit = MetricUnits.MILLISECONDS)
    public Response registerSensor(Sensor sensor) {
        this.producer.produceSensor(sensor);
        return Response.ok().entity("SUCCESS").build();
    }
}
