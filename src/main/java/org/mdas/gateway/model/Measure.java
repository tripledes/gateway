package org.mdas.gateway.model;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;

/**
 * The actual measure, embedding metadata
 */
public class Measure {
    private final Float temperature;
    private final String takenAt;
    private final Sensor sensor;

    @JsonbCreator
    public Measure(@JsonbProperty("temperature") Float temperature, @JsonbProperty("takenAt") String takenAt,
            @JsonbProperty("metadata") Sensor sensor) {
        this.temperature = temperature;
        this.takenAt = takenAt;
        this.sensor = sensor;
    }

    public Float getTemperature() {
        return this.temperature;
    }

    public String getTakenAt() {
        return this.takenAt;
    }

    public Sensor getMetadata() {
        return this.sensor;
    }
}
