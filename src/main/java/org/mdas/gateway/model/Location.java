package org.mdas.gateway.model;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;

/**
 * Location holds sensor's geo-location
 */
public class Location {
    private final Double latitude;
    private final Double longitude;

    @JsonbCreator
    public Location(@JsonbProperty("latitude") Double latitude, @JsonbProperty("longitude") Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double getLongitude() {
        return this.longitude;
    }

    public Double getLatitude() {
        return this.latitude;
    }
}
