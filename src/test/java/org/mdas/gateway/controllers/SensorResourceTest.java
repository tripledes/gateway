package org.mdas.gateway.controllers;

import static io.restassured.RestAssured.given;
import javax.enterprise.inject.Any;
import javax.inject.Inject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mdas.gateway.model.Sensor;
import org.mdas.gateway.model.SensorObjectMother;
import org.mdas.gateway.streams.FakeKafkaResource;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.smallrye.reactive.messaging.connectors.InMemoryConnector;
import io.smallrye.reactive.messaging.connectors.InMemorySink;

/**
 * Testing the Sensor intake resource using FakeKafkaResource
 * (in-memory-connector)
 */
@QuarkusTest
@QuarkusTestResource(FakeKafkaResource.class)
public class SensorResourceTest {

    @Inject
    @Any
    InMemoryConnector connector;

    @AfterEach
    public void clearSink() {
        InMemorySink<Integer> results = connector.sink("sensor-topic");
        results.clear();
    }

    @Test
    public void shouldReturnOkWithSensorData() {
        Sensor newSensor = SensorObjectMother.montcada();
        InMemorySink<Integer> results = connector.sink("sensor-topic");

        given().header("Content-Type", "application/json").body(newSensor).when().post("/register").then()
                .statusCode(200);

        Assertions.assertEquals(1, results.received().size());
    }

    @Test
    public void shouldReturnBadRequestWithWrongData() {
        String wrongData = "This is not sensor data";
        InMemorySink<Integer> results = connector.sink("sensor-topic");

        given().header("Content-Type", "application/json").body(wrongData).when().post("/register").then()
                .statusCode(400);

        Assertions.assertEquals(0, results.received().size());
    }

    @Test
    public void shouldReturnUnsupportedMediaTypeWithNoContentType() {
        Sensor newSensor = SensorObjectMother.montcada();
        InMemorySink<Integer> results = connector.sink("sensor-topic");

        given().body(newSensor).when().post("/register").then().statusCode(415);

        Assertions.assertEquals(0, results.received().size());
    }
}
